#!/usr/bin/env python

################################
#
# manageRequests.py
#
#  Script to create, modify, and clone McM requests.
#
#  author: David G. Sheffield (Rutgers)
#  maintainer: Jose Ruiz (Vanderbilt)
#
################################

import sys
import os
import stat
import argparse
import csv
import re
import pprint
import time
import urllib2
if os.path.exists('/afs/cern.ch/cms/PPD/PdmV/tools/McM/'):
    sys.path.append('/afs/cern.ch/cms/PPD/PdmV/tools/McM/')
    from rest import * # Load class to access McM
from requestClass import * # Load class to store request information
import checkInputScript

def getArguments():
    parser = argparse.ArgumentParser(
        description='Create, modify, and clone McM requests.')

    # Command line flags
    parser.add_argument('file_in')
    parser.add_argument('-c', '--campaign', action='store', dest='campaign',
                        metavar='campaign', help='Set member_of_campaign.')
    parser.add_argument('-p', '--pwg', action='store', dest='pwg', default='EXO',
                        metavar='PWG', help='Set PWG. Defaults to %(default)s. Change default in config.py')
    parser.add_argument('-m', '--modify', action='store_true', dest='doModify',
                        help='Modify existing requests. The CSV file must contain the PrepIds of the requests to be modified.')
    parser.add_argument('--clone', action='store', dest='cloneId', default='',
                        metavar='prepid', help='Clone an existing request by giving its PrepId.')
    parser.add_argument('-d', '--dry', action='store_true', dest='doDryRun',
                        help='Dry run of result. Does not add requests to McM.')
    parser.add_argument('--dev', action='store_true', dest='useDev',
                        help='Use dev/test instance.')
    parser.add_argument('-l', '--lhe', action='store_true', dest='isLHErequest',
                        help='Check dataset when modifying requests. Fail and do not modify name if they conflict. Use for updating GS requests chained to wmLHE and pLHE requests.')
    parser.add_argument('-t', '--tags', action='append', dest='McMTags',
                        metavar='tags', help='Tags to append to request in McM.')

    # Command line flags for local validation tests
    parser.add_argument('--localtest', action='store_true', dest='doLocalTests',
                        help='Flag for local validation tests before McM injection.')
    parser.add_argument('-D', '--dir', action='store', dest='testdir', default='testDir',
                        metavar='testdir', help='Directory to run local validation tests.')
    parser.add_argument('-f', '--fetch', action='store_true', dest='fetch',
                        help='Fetch the local validation test results.')
    parser.add_argument('-n', '--nevents', action='store', dest='testNevents', default=400,
                        metavar='nevents', help='Number of events to run local validation tests.')
    parser.add_argument('--bypasscheck', action='store_true', dest='bypasscheck',
                        help='Flag for local validation tests before McM injection.')

    args_ = parser.parse_args()
    return args_

def checkFile(file_):
    # Check that CSV file exists
    if not os.path.isfile(file_):
        print "Error: File {0} does not exist.".format(file_)
        sys.exit(1)

def makeExe(fname):
    st = os.stat(fname)
    os.chmod(fname, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

def checkPWG(pwg_):
    pwg_list = ['B2G', 'BPH', 'BTW', 'EGM', 'EWK', 'EXO', 'FSQ', 'FWD', 'HCA',
                'HIG', 'HIN', 'JME', 'L1T', 'MUO', 'QCD', 'SMP', 'SUS', 'TAU',
                'TOP', 'TRK', 'TSG']
    # Check that PWG is valid
    if pwg_ not in pwg_list:
        print "Error: {0} is not a recognized PWG.".format(pwg_)
        if pwg_ == 'XXX':
            print "Change the default value for flag -p to your PWG by modifying the variable defaultPWG on line 23."
        sys.stdout.write("Options are:")
        for iPWG in pwg_list:
            sys.stdout.write(" ")
            sys.stdout.write(iPWG)
        sys.stdout.write("\n")
        sys.exit(2)

def checkNotCreate(doModify_, cloneId_):
    # Check that script isn't being asked to both modify and clone a request
    doClone = False
    if cloneId_ != "":
        doClone = True
    if doModify_ and doClone:
        print "Error: cannot both --modify and --clone."
        sys.exit(6)
    return doModify_ or doClone # Return variable to use in fillFields()

def exitDuplicateField(file_in_, field_):
    print "Error: File {0} contains multiple instances of the field {1}".format(
        file_in_, field_)
    sys.exit(3)

def getFields(csvfile_):
    field_indices = {}
    field_candidates = ['dataset', 'mcdbid', 'cross section', 'events', 'fragment', 'time per event', 'size per event', 'tag', 'generator', 'campaign', 'sequences customize', 'gridpack', 'gridpack cards url', 'mcm tag', 'filter efficiency', "filter efficiency err", "match efficiency", "match efficiency err", "pwg", 'campaign', 'prepid', 'sequences customize', 'process string', 'notes', 'sequences beamspot', 'sequences magfield', 'jobid']

    synonym_list = {
        "dataset":["dataset name", "dataset", "name"],
        "mcdbid":["eos", "mcdbid"],
        "cross section":['cross section [pb]', 'cross section (pb)', 'cross section', 'cs', 'cs [pb]', 'cs (pb)', 'xsec', 'xsec [pb]', 'xsec (pb)'],
        "events":['total events', 'events', 'number of events'],
        "fragment":['fragment name', 'generator fragment name', 'fragment'],
        "time per event":['time per event [s]', 'time per event', 'time per event (s)', 'time', 'time [s]', 'time (s)'],
        "size per event":['size per event [kb]', 'size per event', 'size per event (kb)', 'size', 'size [kb]', 'size (kb)'],
        "tag":['tag', 'fragment tag', 'sha', 'sha-1'],
        "generator":['generator', 'generators'],
        "campaign":['campaign', 'member of campaign'],
        "sequences customize":['sequences customise', 'sequences customize'],
        "gridpack":['gridpack location', 'gridpack'],
        "gridpack cards url":['gridpack cards url', 'cards url', 'gridpack cards location', 'cards location', 'gridpack url'],
        "mcm tag":['mcm tag', 'mcm tags'],
        "match efficiency":["match efficiency", "matching efficiency", "match eff", "matching eff"],
        "match efficiency err":["match efficiency err", "matching efficiency err", "match eff eff", "matching eff eff"],
        "filter efficiency":["filter efficiency", "filtering efficiency", "filter eff", "filtering eff"],
        "filter efficiency err":["filter efficiency err", "filtering efficiency err", "filter eff eff", "filtering eff eff"],
    }

    # Process header row
    header = csv.reader(csvfile_).next()
    for ind, field in enumerate(header):
        field = field.lower()

        # Check for synonyms
        for field_rename, field_synonyms in synonym_list.iteritems():
            if field in field_synonyms:
                field = field_rename

        # Field is in the whitelist?
        if not field in field_candidates:
            raise ValueError("Unknown header field: {} in header {}".format(field, header))

        # Duplicate?
        if field in field_indices:
            raise ValueError("Duplicate header field: {} in header {}".format(field, header))

        field_indices[field] = ind

    return field_indices

def fetchFragment(file_):

    if "http" in file_:
        url = file_
        print 'Fetching '+ url
        FragmentFetched = urllib2.urlopen(url).read()
    elif os.path.exists(file_):
        FObject = open(file_,"r")
        FragmentFetched = FObject.read()
        FObject.close()
    else:
        print "Error: Cannot find the fragment file. It should be located in one of the followings :\n\t1) github (with full raw address)\n\t2) lxplus\n\t3) {}".format(os.getcwd())
        sys.exit(1)

    return FragmentFetched


def createLHEProducer(gridpack, cards, fragment, tag):
    code = """import FWCore.ParameterSet.Config as cms

externalLHEProducer = cms.EDProducer("ExternalLHEProducer",
    args = cms.vstring('{0}'),
    nEvents = cms.untracked.uint32(5000),
    numberOfParameters = cms.uint32(1),
    outputFile = cms.string('cmsgrid_final.lhe'),
    scriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)""".format(gridpack)

    if cards != "":
        code += """

# Link to cards:
# {0}
""".format(cards)

    if fragment != "":
        #gen_fragment_url = "https://raw.githubusercontent.com/cms-sw/genproductions/{0}/{1}".format(
        #    tag, fragment.split("Configuration/GenProduction/")[1])
        gen_fragment_url = fragment
        #gen_fragment = urllib2.urlopen(gen_fragment_url).read()
        gen_fragment = fetchFragment(fragment)

        if "ExternalLHEProducer" in gen_fragment:
            raise ValueError("The analyzer fragment already has an ExternalLHEProducer. Please remove it before proceeding. Otherwise, the fragment will have two ExternalLHEProducers.")

        code += """
{0}

# Link to generator fragment:
# {1}
""".format(gen_fragment, gen_fragment_url)
    #if "\t" in code: 
    #print code#.split("\n")[:10]
    #print repr(code)
    return code


def fillFields(csvfile, fields, campaign, PWG, notCreate_, McMTags):
    requests = [] # List containing request objects
    num_requests = 0

    # modify Total Events (multiply 0.5) for 2016 Ultra Legacy (RunII Legacy) samples since it is split into 2, before and after APV fix
    modifyTotalEvents = False
    if campaign in ["RunIISummer20UL16GEN", "RunIISummer20UL16GENAPV", "RunIISummer20UL16wmLHEGEN", "RunIISummer20UL16wmLHEGENAPV"]:
        modifyTotalEvents = True

    # Required fields
    #required_fields = ["fragment", "dataset", "events"]
    #for required_field in required_fields:
    #    if not required_field in fields:
    #        raise ValueError("Missing required field {}. Please make sure it's in the header.".format(required_field))

    for row in csv.reader(csvfile):
        if "fragment" in fields:
            if "http" in row[fields["fragment"]]:
                time.sleep(1)
                print num_requests % 15
                if num_requests % 15 == 14:
                    print "[fillFields] INFO : Sleeping to avoid HTTP Error 429: Too Many Requests"
                    time.sleep(20)
        if row[0].startswith("#"):
            continue
        num_requests += 1
        tmpReq = Request()

        # Set default values here, before parsing CSV file
        tmpReq.setKeepOutput(False)

        if "dataset" in fields:
            tmpReq.setDataSetName(row[fields["dataset"]])

        if "mcdbid" in fields:
            tmpReq.setMCDBID(row[fields["mcdbid"]])
        elif not notCreate_:
            # For some reason, MCM complains about wmLHEGS with negative MCDBID. 
            # David thinks there's a bug somewhere, but this should fix the problem.
            if campaign in ["RunIISummer15wmLHEGS", "RunIIFall17wmLHEGS", "RunIIFall18wmLHEGS", "RunIISummer20UL16wmLHEGEN", "RunIISummer20UL16wmLHEGENAPV", "RunIISummer20UL17wmLHEGEN", "RunIISummer20UL18wmLHEGEN"]:
                tmpReq.setMCDBID(0)
            else:
                tmpReq.setMCDBID(-1)
        if "cross section" in fields:
            tmpReq.setCS(row[fields["cross section"]])
        elif not notCreate_:
            tmpReq.setCS(1.0)

        if "events" in fields:
            if modifyTotalEvents == True:# RunIISummer20UL16 and RunIISummer20UL16APV separated
                tmpReq.setEvts(int(int(row[fields["events"]])*0.5))
            else:
                tmpReq.setEvts(row[fields["events"]])
        if "campaign" in fields:
            campaign = row[fields["campaign"]]
            tmpReq.setCamp(campaign)
        elif campaign is not None:
            tmpReq.setCamp(campaign)

        if "time per event" in fields:
            tmpReq.setTime(row[fields["time per event"]])

        if "size per event" in fields:
            tmpReq.setSize(row[fields["size per event"]])

        if "tag" in fields:
            tmpReq.setTag(row[fields["tag"]])

        if "generator" in fields:
            tmpReq.setGen(row[fields["generator"]]) # Multiple generators separated by spaces

        if "filter efficiency" in fields:
            tmpReq.setFiltEff(row[fields["filter efficiency"]])
        elif not notCreate_:
            tmpReq.setFiltEff(1.0)

        if "filter efficiency err" in fields:
            tmpReq.setFiltEffErr(row[fields["filter efficiency err"]])
        elif not notCreate_:
            tmpReq.setFiltEffErr(0.0)

        if "match efficiency" in fields:
            tmpReq.setMatchEff(row[fields["match efficiency"]])
        elif not notCreate_:
            tmpReq.setMatchEff(1.0)

        if "match efficiency err" in fields:
            tmpReq.setMatchEffErr(row[fields["match efficiency err"]])
        elif not notCreate_:
            tmpReq.setMatchEffErr(0.0)

        if "pwg" in fields:
            tmpReq.setPWG(row[fields["pwg"]])
        elif not notCreate_:
            tmpReq.setPWG(PWG)

        if "prepid" in fields:
            tmpReq.setPrepId(row[fields["prepid"]])

        if "sequences customize" in fields:
            tmpReq.setSequencesCustomise(row[fields["sequences customize"]])

        if "process string" in fields:
            tmpReq.setProcessString(row[fields["process string"]])

        if "gridpack" in fields:
            this_gridpack = row[fields["gridpack"]]
            tmpReq.setGridpack(this_gridpack)

            if "gridpack cards url" in fields:
                this_gridpack_cards_url = row[fields["gridpack cards url"]]
            else:
                this_gridpack_cards_url = ""
            if "fragment" in fields:
                this_fragment = row[fields["fragment"]]
                tmpReq.setFragPyFile(this_fragment)
            else:
                this_fragment = ""
            if "tag" in fields:
                this_tag = row[fields["tag"]]
            else:
                this_tag = ""
            tmpReq.setMcMFrag(createLHEProducer(this_gridpack, this_gridpack_cards_url, this_fragment, this_tag))
        else:
            if "fragment" in fields:
                fragmentPyFile = row[fields["fragment"]]
                fragment = fetchFragment(fragmentPyFile)
                if fragment:
                    tmpReq.setMcMFrag(fragment)
                    tmpReq.setFragPyFile(fragmentPyFile)
                else:
                    print "[fillFields] WARNING : Failed to get fragment " + row[fields["fragment"]] + "!"
                    sys.exit(1)

        if "notes" in fields:
            tmpReq.setNotes(row[fields[20]])

        if "mcm tag" in fields:
            tmpReq.setMcMTag(row[fields[21]].split(" "))
        elif McMTags is not None:
            tmpReq.setMcMTag(McMTags)

        if "sequences beamspot" in fields:
            tmpReq.setSequencesBeamspot(row[fields[22]])
        if "sequences magfield" in fields:
            tmpReq.setSequencesMagField(row[fields[23]])
        requests.append(tmpReq)
    return requests, num_requests

def createRequests(requests, num_requests, doDryRun, useDev):
    # Create new requests based on campaign and PWG
    # print "Is dev being used? -----------------------> ", useDev
    mcm = McM(dev=useDev) # Get McM connection

    if not doDryRun:
        print "Adding {0} requests to McM.".format(num_requests)
    else:
        print "Dry run. {0} requests will not be added to McM.".format(
            num_requests)
    for reqFields in requests:
        if not reqFields.useCamp():
            print "A campaign is needed for new requests."
            continue

        # Create new request's dictionary
        new_req = {'pwg': reqFields.getPWG(),
                   'member_of_campaign': reqFields.getCamp(),
                   'mcdb_id': reqFields.getMCDBID()}
        # Fill dictionary with fields
        if reqFields.useDataSetName():
            new_req['dataset_name'] = reqFields.getDataSetName()
        if reqFields.useEvts():
            new_req['total_events'] = reqFields.getEvts()
        if reqFields.useMcMFrag():
            new_req['fragment'] = reqFields.getMcMFrag()
            #if type(new_req['fragment'])==type(reqFields.getMcMFrag()): print "Same type of McMFrag object is propagated"
            #if new_req['fragment']==reqFields.getMcMFrag(): print "Same McMFrag object is propagated"
        else:
            if reqFields.useFrag():
                new_req['name_of_fragment'] = reqFields.getFrag()
            if reqFields.useTag():
                new_req['fragment_tag'] = reqFields.getTag()
        if reqFields.useTime():
            new_req['time_event'] = [reqFields.getTime()]
        if reqFields.useSize():
            new_req['size_event'] = [reqFields.getSize()]
        if reqFields.useGen():
            new_req['generators'] = [reqFields.getGen()]
        if reqFields.useProcessString():
            new_req['process_string'] = reqFields.getProcessString()
        if reqFields.useNotes():
            new_req['notes'] = reqFields.getNotes()
        if reqFields.useMcMTag():
            new_req['tags'] = reqFields.getMcMTag()
        #print new_req
        
        if not doDryRun:
            #print "DEBUG 1 -----------------------> ", "Dictionary prepared:", new_req
            #pprint.pprint(new_req)
            answer = mcm.put('requests', new_req) # Create request
            #print "DEBUG 2 -----------------------> ", answer
            #print "DEBUG 3 -----------------------> ", answer['results']
            #print "DEBUG 4 -----------------------> ", answer['prepid']
            if not "results" in answer:
                print "Something is wrong! Here is the response from MCM:"
                print answer
                sys.exit(1)
            if answer['results']:
                # Cannot fill generator parameters while creating a new request
                # Modify newly created request with generator parameters
                # Get newly created request
                print "Succesfully created request:", answer['prepid']
                mcm = McM(dev=useDev)
                mod_req = mcm.get('requests', answer['prepid'])
                # Fill generator parameters
                mod_req['generator_parameters'][0]['cross_section'] \
                    = reqFields.getCS()
                mod_req['generator_parameters'][0]['filter_efficiency'] \
                    = reqFields.getFiltEff()
                mod_req['generator_parameters'][0]['filter_efficiency_error'] \
                    = reqFields.getFiltEffErr()
                mod_req['generator_parameters'][0]['match_efficiency'] \
                    = reqFields.getMatchEff()
                mod_req['generator_parameters'][0]['match_efficiency_error'] \
                    = reqFields.getMatchEffErr()
                if reqFields.useSequencesCustomise():
                    mod_req['sequences'][0]['customise'] = reqFields.getSequencesCustomise()
                if reqFields.useSequencesBeamspot():
                    mod_req['sequences'][0]['beamspot'] = reqFields.getSequencesBeamspot()
                if reqFields.useSequencesMagField():
                    mod_req['sequences'][0]['magField'] = reqFields.getSequencesMagField()
                # Update request with generator parameters and sequences
                update_answer = mcm.update('requests', mod_req)
                if update_answer['results']:
                    print "\033[0;32m{0} created\033[0;m".format(answer['prepid'])
                else:
                    print "\033[0;33m{0} created but generator parameters not set\033[0;m".format(
                        answer['prepid'])
            else:
                if reqFields.useDataSetname():
                    print "\033[0;31m{0} failed to be created\033[0;m".format(
                        reqFields.getDataSetName())
                else:
                    print "\033[0;31mA request has failed to be created\033[0;m"
        else:
            if reqFields.useDataSetName():
                print "\033[0;31m{0} not created\033[0;m".format(
                    reqFields.getDataSetName())
            else:
                print "\033[0;31mrequest not created\033[0;m"
            #pprint.pprint(new_req)

def modifyRequests(requests, num_requests, doDryRun, useDev, isLHErequest):
    # Modify existing request based on PrepId
    mcm = McM(dev=useDev) # Get McM connection

    if not doDryRun:
        print "Modifying {0} requests to McM.".format(num_requests)
    else:
        print "Dry run. {0} requests will not be modified in McM.".format(
            num_requests)
    for reqFields in requests:
        # Get request from McM
        if isLHErequest:
            if not reqFields.useDataSetName():
                print "\033[0;31mDataset name missing\033[0;m"
                continue
            elif not reqFields.useCamp():
                print "\033[0;31m{0} modification failed. Must provide campaign.\033[0;m".format(
                    reqFields.getDataSetName())
                continue
            query_string = "dataset_name={0}&member_of_campaign={1}".format(
                reqFields.getDataSetName(), reqFields.getCamp())
            failed_to_get = True
            for tries in range(3):
                time.sleep(0.1)
                mcm = McM(dev=useDev)
                mod_req_list = mcm.get('requests', query=query_string)
                if mod_req_list is not None:
                    failed_to_get = False
                    break
            if failed_to_get:
                print "\033[0;31m{0} modification failed. Could not get request from McM.\033[0;m".format(
                    reqFields.getDataSetName())
                continue
            if len(mod_req_list) > 1:
                print "\033[0;31m{0} modification failed. Too many requests match query.\033[0;m".format(
                    reqFields.getDataSetName())
                continue
            elif len(mod_req_list) == 0:
                print "\033[0;31m{0} modification failed. No requests match query.\033[0;m".format(
                    reqFields.getDataSetName())
                continue
            mod_req = mod_req_list[0]
        else:
            if not reqFields.usePrepId():
                print "\033[0;31mPrepId is missing.\033[0;m"
                continue
            time.sleep(0.1)
            mcm = McM(dev=useDev)
            mod_req = mcm.get('requests', reqFields.getPrepId())

        if reqFields.useDataSetName() and not isLHErequest:
            mod_req['dataset_name'] = reqFields.getDataSetName()
        if reqFields.useMCDBID():
            mod_req['mcdb_id'] = reqFields.getMCDBID()
        if reqFields.useEvts():
            mod_req['total_events'] = reqFields.getEvts()
        if reqFields.useMcMFrag():
            mod_req['fragment'] = reqFields.getMcMFrag()
            #mod_req['fragment'] = reqFields.getMcMFrag().replace('"""',"'")
        else:
            #print "Modifying non LHE requests"
            if reqFields.useFrag():
                mod_req['name_of_fragment'] = reqFields.getFrag()#.replace('"""',"'")
            if reqFields.useTag():
                mod_req['fragment_tag'] = reqFields.getTag()
        if reqFields.useTime():
            mod_req['time_event'] = [reqFields.getTime()]
            #print "Debugging update request: Time=", reqFields.getTime(), type(reqFields.getTime()), mod_req['time_event'], type(mod_req['time_event'])
        if reqFields.useSize():
            mod_req['size_event'] = [reqFields.getSize()]
            #print "Debugging update request: Size=", reqFields.getSize()
        if reqFields.useGen():
            mod_req['generators'] = [reqFields.getGen()]
        if (reqFields.useCS() or reqFields.useFiltEff()
            or reqFields.useFiltEffErr() or reqFields.useMatchEff()
            or reqFields.useMatchEffErr()):# and mod_req['generator_parameters'] == []:
            print "Resetting generator_parameters"
            mod_req['generator_parameters'] = [{'match_efficiency_error': 0.0,
                                                'match_efficiency': 1.0,
                                                'filter_efficiency': 1.0,
                                                'version': 0,
                                                'cross_section': 1.0,
                                                'filter_efficiency_error': 0.0}]
        if reqFields.useCS():
            mod_req['generator_parameters'][0]['cross_section'] \
                = reqFields.getCS()
        if reqFields.useFiltEff():
            mod_req['generator_parameters'][0]['filter_efficiency'] \
                = reqFields.getFiltEff()
        if reqFields.useFiltEffErr():
            mod_req['generator_parameters'][0]['filter_efficiency_error'] \
                = reqFields.getFiltEffErr()
        if reqFields.useMatchEff():
            mod_req['generator_parameters'][0]['match_efficiency'] \
                = reqFields.getMatchEff()
        if reqFields.useMatchEffErr():
            mod_req['generator_parameters'][0]['match_efficiency_error'] \
                = reqFields.getMatchEffErr()
        if reqFields.useSequencesCustomise():
            mod_req['sequences'][0]['customise'] \
                = reqFields.getSequencesCustomise()
        if reqFields.useSequencesBeamspot():
            mod_req['sequences'][0]['beamspot'] = reqFields.getSequencesBeamspot()
        if reqFields.useSequencesMagField():
            mod_req['sequences'][0]['magField'] = reqFields.getSequencesMagField()
        if reqFields.useProcessString():
            mod_req['process_string'] = reqFields.getProcessString()
        if reqFields.useNotes():
            mod_req['notes'] = reqFields.getNotes()
        if reqFields.useMcMTag():
            mod_req['tags'] += reqFields.getMcMTag()

        # Don't update if the local test job failed
        if mod_req['time_event'] == 0 or mod_req['size_event'] == 0:
            print "ERROR : For prepid {}, time_event={}, size_event={}. Skipping this request.".format(reqFields.getPrepId(), mod_req['time_event'], mod_req['size_event'])
            return
            
        #Avoiding to have unset generator parameters
        if mod_req['generator_parameters'][0]['cross_section'] < 0: mod_req['generator_parameters'][0]['cross_section'] = 1.0
        if mod_req['generator_parameters'][0]['filter_efficiency'] < 0: mod_req['generator_parameters'][0]['filter_efficiency'] = 1.0
        if mod_req['generator_parameters'][0]['filter_efficiency_error'] < 0: mod_req['generator_parameters'][0]['filter_efficiency_error'] = 0.0
        if mod_req['generator_parameters'][0]['match_efficiency'] < 0: mod_req['generator_parameters'][0]['match_efficiency'] = 1.0
        if mod_req['generator_parameters'][0]['match_efficiency_error'] < 0: mod_req['generator_parameters'][0]['match_efficiency_error'] = 0.0

        if not doDryRun:
            #mod_req['fragment']=mod_req['fragment'].replace('"""',"'")
            answer = mcm.update('requests', mod_req) # Update request
            #print "mod_req object: ", mod_req
            #print "Debugging update request: McM_Answer=", answer
            if answer['results']:
                if not isLHErequest:
                    print "\033[0;32m{0} modified\033[0;m".format(
                        reqFields.getPrepId())
                else:
                    print "\033[0;32m{0} in {1} modified\033[0;m".format(
                        reqFields.getDataSetName(), reqFields.getCamp())
            else:
                if not isLHErequest:
                    print "\033[0;31m{0} failed to be modified\033[0;m".format(
                        reqFields.getPrepId())
                else:
                    print "\033[0;31m{0} failed to be modified\033[0;m".format(
                        reqFields.getDataSetName())
        else:
            if not isLHErequest:
                print "\033[0;31m{0} not modified\033[0;m".format(
                    reqFields.getPrepId())
                pprint.pprint(mod_req)
            else:
                print "\033[0;31m{0} not modified\033[0;m".format(
                    reqFields.getDataSetName())
                pprint.pprint(mod_req)


def cloneRequests(requests, num_requests, doDryRun, useDev, cloneId_):
    # Create new requests be cloning an old one based on PrepId
    mcm = McM(dev=useDev) # Get McM connection

    if not doDryRun:
        print "Adding {0} requests to McM using clone.".format(num_requests)
    else:
        print "Dry run. {0} requests will not be added to McM using clone.".format(
            num_requests)
    for reqFields in requests:
        mcm = McM(dev=useDev)
        clone_req = mcm.get('requests', cloneId_) # Get request to clone
        if reqFields.useDataSetName():
            clone_req['dataset_name'] = reqFields.getDataSetName()
        if reqFields.useMCDBID():
            clone_req['mcdb_id'] = reqFields.getMCDBID()
        if reqFields.useEvts():
            clone_req['total_events'] = reqFields.getEvts()
        if reqFields.useMcMFrag():
            clone_req['fragment'] = reqFields.getMcMFrag()
        else:
            if reqFields.useFrag():
                clone_req['name_of_fragment'] = reqFields.getFrag()
            if reqFields.useTag():
                clone_req['fragment_tag'] = reqFields.getTag()
        if reqFields.useTime():
            clone_req['time_event'] = [reqFields.getTime()]
        if reqFields.useSize():
            clone_req['size_event'] = [reqFields.getSize()]
        if reqFields.useGen():
            clone_req['generators'] = [reqFields.getGen()]
        if reqFields.useCS():
            clone_req['generator_parameters'][0]['cross_section'] \
                = reqFields.getCS()
        if reqFields.useFiltEff():
            clone_req['generator_parameters'][0]['filter_efficiency'] \
                = reqFields.getFiltEff()
        if reqFields.useFiltEffErr():
            clone_req['generator_parameters'][0]['filter_efficiency_error'] \
                = reqFields.getFiltEffErr()
        if reqFields.useMatchEff():
            clone_req['generator_parameters'][0]['match_efficiency'] \
                = reqFields.getMatchEff()
        if reqFields.useMatchEffErr():
            clone_req['generator_parameters'][0]['match_efficiency_error'] \
                = reqFields.getMatchEffErr()
        if reqFields.useSequencesCustomise():
            clone_req['sequences'][0]['customise'] \
                = reqFields.getSequencesCustomise()
        if reqFields.useSequencesBeamspot():
            clone_req['sequences'][0]['beamspot'] = reqFields.getSequencesBeamspot()
        if reqFields.useSequencesMagField():
            clone_req['sequences'][0]['magField'] = reqFields.getSequencesMagField()
        if reqFields.useProcessString():
            clone_req['process_string'] = reqFields.getProcessString()
        if reqFields.useNotes():
            clone_req['notes'] = reqFields.getNotes()
        if reqFields.useMcMTag():
            clone_req['tags'] += reqFields.getMcMTag()

        if not doDryRun:
            answer = mcm.clone(cloneId_, clone_req) # Clone request
            if answer['results']:
                print "\033[0;32m{0} created using clone\033[0;m".format(
                    answer['prepid'])
            else:
                if reqFields.useDataSetName():
                    print "\033[0;31m{0} failed to be created using clone\033[0;m".format(
                        reqFields.getDataSetName())
                else:
                    print "\033[0;31mrequest failed to be created using clone\033[0;m"
        else:
            if reqFields.useDataSetName():
                print "\033[0;31m{0} not created using clone\033[0;m".format(
                    reqFields.getDataSetName())
            else:
                print "\033[0;31mrequest not created using clone\033[0;m"
            pprint.pprint(clone_req)


def runLocalTests(requests, fields, testdir, testNevents):

    if os.path.exists(testdir):
        print "Error: {} ".format(testdir)+"already exists"
        sys.exit(1)

    print "Build up codes for local validation tests"
    os.system("sleep 3")

    os.system("mkdir -p {}/genFragments/".format(testdir))
    os.system("mkdir -p {}/logs/".format(testdir))
    nameSubmitCondor = "submit_condor_{}.sh".format(testdir)
    submitCondor = open(nameSubmitCondor, "w")
    submitCondor.write("#!/bin/bash\n")
    submitCondor.write("cd {}/\n".format(testdir))

    # Preparing the test results CSV file
    inputCSV = open("{}/input.csv".format(testdir), "w")
    writeCSV = csv.writer(inputCSV)

    firstRowCSV = []
    field_essentials_CSV = ["dataset", "fragment", "events", "generator"]
    field_additionals_CSV = ["gridpack"]#, "time per event","size per event"]

    # Fill in the first row of the test results CSV file
    for field in field_essentials_CSV:
        if field in fields.keys(): firstRowCSV.append(field)
    for field in field_additionals_CSV:
        if field in fields.keys(): firstRowCSV.append(field)

    writeCSV.writerow(firstRowCSV)

    # Fill in the rest of the rows of the test results CSV file : requested samples
    for reqFields in requests:
        reqRowCSV = []
        reqRowDataSetName = reqFields.getDataSetName()

        reqRowCSV.append(reqFields.getDataSetName())
        reqRowCSV.append(reqFields.getFragPyFile())
        reqRowCSV.append(reqFields.getEvts())
        reqRowCSV.append(reqFields.getGen())
        if reqFields.useGridpack():
          reqRowCSV.append(reqFields.getGridpack())
        writeCSV.writerow(reqRowCSV)

        reqFragPy = open(testdir+"/genFragments/"+reqRowDataSetName+".py", "w")
        reqFragment = reqFields.getMcMFrag()
        reqFragPy.write(reqFragment)
        reqFragPy.close()

        os.system("cp packages/submit_condor.jds {0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###EXECUTABLE###|run_condor_{}.sh|g' ".format(reqRowDataSetName)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###TRANSFER_INPUT_FILES###|{}.py|g' ".format(reqRowDataSetName)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###JOBBATCHNAME###|{}|g' ".format(testdir)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###OUTPUT###|condor_{}.stdout|g' ".format(reqRowDataSetName)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###ERROR###|condor_{}.stderr|g' ".format(reqRowDataSetName)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###LOG###|condor_{}.stdlog|g' ".format(reqRowDataSetName)+"{0}/submit_condor_{1}.jds".format(testdir,reqRowDataSetName))

        os.system("cp packages/run_condor.sh {0}/run_condor_{1}.sh".format(testdir,reqRowDataSetName))
        os.system("sed -i 's|###DATASETNAME###|{}|g' ".format(reqRowDataSetName)+"{0}/run_condor_{1}.sh".format(testdir,reqRowDataSetName))
        if "ExternalLHEProducer" in reqFragment: useExtLHE = "LHE,"
        else: useExtLHE = ""
        os.system("sed -i 's|###LHE###|{}|g' ".format(useExtLHE)+"{0}/run_condor_{1}.sh".format(testdir,reqRowDataSetName))
        if int(testNevents) < 400: testNevents = "500"
        os.system("sed -i 's|###TEST_NEVENTS###|{}|g' ".format(testNevents)+"{0}/run_condor_{1}.sh".format(testdir,reqRowDataSetName))

        submitCondor.write("condor_submit submit_condor_{}.jds\n".format(reqRowDataSetName))

        print reqRowCSV

    inputCSV.close()  
    submitCondor.close()

    # make executable
    makeExe(nameSubmitCondor)

    print "Execute the command to submit the local validation jobs :"
    print " >>       ./{}".format(nameSubmitCondor)


def fetchLocalTests(requests, file_in):

    testdir = file_in.replace("/input.csv","")

    if not os.path.exists(testdir):
        print "Error: {} ".format(testdir)+"does not exist"
        sys.exit(1)

    else:
        if not os.path.exists("{}/input.csv".format(testdir)):
            print "Error: {}/input.csv ".format(testdir)+"does not exist"
            sys.exit(1)
        else:
            print "Fetching local validation test results from {}".format(testdir)

    # Variables in stdout
    regex_timetotal = re.compile('Total job time: (\d*\.\d*) s') 
    regex_timeperevent = re.compile('Time per event: (\d*\.\d*) s')
    regex_sizeperevent = re.compile('Size per event: (\d*\.\d*) kB')
    regex_eventsafter = re.compile('Total events: (\d*)') # Events after filtering/matching
    regex_eventsbefore = re.compile('Running total events : (\d*)') # Events before filtering/matching 

    # Variables in stderr
    regex_filtereff = re.compile("Filter efficiency .*=.*= (?P<n1>[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?) \+- (?P<n2>[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?).*\[TO BE USED IN MCM\]")
    regex_matcheff = re.compile("Matching efficiency = (?P<n1>[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?) \+/- (?P<n2>[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?).*\[TO BE USED IN MCM\]")

    inputCSV = open("{}/input.csv".format(testdir))
    testResultsCSV = open("{}/test_results.csv".format(testdir), "w")

    readCSV = csv.reader(inputCSV)
    writeCSV = csv.writer(testResultsCSV)

    nextRowCSV = readCSV.next()
    nextRowCSV.append("time per event")
    nextRowCSV.append("size per event")
    nextRowCSV.append("match efficiency")
    nextRowCSV.append("filter efficiency")

    writeCSV.writerow(nextRowCSV)

    datasetnameWithErrors = []
    for reqFields in requests:

        reqRowDataSetName = reqFields.getDataSetName()
        print "Fetching local validation test results for {}".format(reqRowDataSetName)

        timetotal = -1.
        timeperevent = -1.
        sizeperevent = -1.
        eventsafter = -1.
        eventsbefore = -1.
        matcheff = -1.
        filtereff = -1.

        fileToSearch = open("{0}/logs/condor_{1}.stdout".format(testdir, reqRowDataSetName), "r")
        for line in fileToSearch:
            if not regex_timetotal.search(line) is None:
                    timetotal = float(regex_timetotal.search(line).group(1))
            if not regex_timeperevent.search(line) is None:
                timeperevent = float(regex_timeperevent.search(line).group(1))
            if not regex_sizeperevent.search(line) is None:
                sizeperevent = float(regex_sizeperevent.search(line).group(1))
            if not regex_eventsafter.search(line) is None:
                eventsafter = float(regex_eventsafter.search(line).group(1))
            if not regex_eventsbefore.search(line) is None:
                eventsbefore = float(regex_eventsbefore.search(line).group(1))
        fileToSearch.close()

        fileToSearch = open("{0}/logs/condor_{1}.stderr".format(testdir, reqRowDataSetName), "r")
        for line in fileToSearch:
            if not regex_matcheff.search(line) is None: 
                matcheff = float(regex_matcheff.search(line).group("n1"))
            if not regex_filtereff.search(line) is None:
                filtereff = float(regex_filtereff.search(line).group("n1"))
        if matcheff < 0: matcheff = 1.0
        if filtereff < 0: filtereff = 1.0
        fileToSearch.close()

        time_per_event_inCSV = -1.
        size_per_event_inCSV = -1.
        match_eff_inCSV = -1.
        filter_eff_inCSV = -1.

        if eventsafter > 0:
            time_per_event_inCSV = ( ( (float(timetotal)/float(eventsafter)) + float(timeperevent) ) / 2.)
            size_per_event_inCSV = sizeperevent

        if matcheff > 0 and filtereff > 0:
            if abs(matcheff*filtereff - eventsafter/eventsbefore) > 0.05:
                print " >>       Warning: Total efficiency {0} does not match matchEff*filterEff = {1}*{2}.".format(eventsafter/eventsbefore, matcheff, filtereff)
            match_eff_inCSV = matcheff
            filter_eff_inCSV = filtereff

        if (time_per_event_inCSV < 0) or (size_per_event_inCSV <0):
            print " >>       Error: Test result not fetched for {}".format(reqRowDataSetName)
            datasetnameWithErrors.append(reqRowDataSetName)

        nextRowCSV = readCSV.next()
        nextRowCSV.append(time_per_event_inCSV)
        nextRowCSV.append(size_per_event_inCSV)
        nextRowCSV.append(match_eff_inCSV)
        nextRowCSV.append(filter_eff_inCSV)
        writeCSV.writerow(nextRowCSV)

    inputCSV.close()
    testResultsCSV.close()

    os.system("sleep 1")
    if len(datasetnameWithErrors) > 0:
        nameResubmitCondor = "resubmit_condor_{}.sh".format(testdir)
        resubmitCondor = open(nameResubmitCondor, "w")
        resubmitCondor.write("#!/bin/bash\n")
        resubmitCondor.write("cd {}/\n".format(testdir))

        for rerunDataSetName in datasetnameWithErrors:
            resubmitCondor.write("condor_submit submit_condor_{}.jds\n".format(rerunDataSetName))
        resubmitCondor.close()
        makeExe(nameResubmitCondor)

        print "Execute the command to resubmit the failed local validation jobs :"
        print " >>       ./{}".format(nameResubmitCondor)
    else:
        print "#####################################################################################"
        print "Printing out the CSV file with local validation test results."
        os.system("cat {}/test_results.csv".format(testdir))
        print "#####################################################################################"
        print "CSV file with local validation test results are stored in {}/test_results.csv.".format(testdir)
        print "Make a pull request for sample requests with this CSV and adoc file in gitlab : https://gitlab.cern.ch/cms-exo-mci/EXO-MCsampleRequests."
        print "adoc file is stored in packages/request.adoc"
        print "#####################################################################################"



def main():

    args = getArguments() # Setup flags and get arguments
    checkPWG(args.pwg) # Make sure PWG is an actual PWG
    # Check that script is not asked to both modify and clone
    # Store variable that is true if script is asked to modify or clone
    notCreate = checkNotCreate(args.doModify, args.cloneId)
    checkFile(args.file_in) # Ensure CSV file exists

    csvfile = open(args.file_in, 'r') # Open CSV file
    fields = getFields(csvfile) # Get list of field indices
    # Fill list of request objects with fields from CSV and get number of requests
    requests, num_requests = fillFields(csvfile, fields, args.campaign, args.pwg, notCreate, args.McMTags)

    if not args.bypasscheck:# Check input CSV files
        checkResult = checkInputScript.checkInputCSV(requests, fields, args.testdir, args.file_in)
        if not checkResult:
            sys.exit(1) #Should re-execute since check CSV result is False
    else:# Bypass check
        print "Bypassing input CSV file checks. McM injection will eventually require changes in the CSV files anyways."

    if args.doLocalTests:
        if not args.fetch:# Run local validation test before McM injection
            runLocalTests(requests, fields, args.testdir, args.testNevents)
        else:# Fetch the local validation test results
            fetchLocalTests(requests, args.file_in)
        sys.exit(0)
    else:
        userid = os.getlogin()
        contacts = ["shjeon", "mkrohn", "rasharma", "jvora", "youngwan", "madecaro", "kwei"]

        if not (userid in contacts):
            print "Error: User {0} does not exist in the contact list {1}.".format(userid,contacts)
            sys.exit(1)

        checkGridpacks = checkInputScript.checkGridpack(requests)
        if not checkGridpacks == 0:
            print "Error : {0} gridpacks in {1} are not placed in CVMFS area.".format(checkGridpacks, args.file_in)
            sys.exit(1)

    if args.useDev:
        print "Using dev/test instance."

    if args.doModify:
        # Modify existing requests
        modifyRequests(requests, num_requests, args.doDryRun, args.useDev,
                       args.isLHErequest)
    elif args.cloneId != "":
        # Create new requests using clone
        cloneRequests(requests, num_requests, args.doDryRun, args.useDev,
                      args.cloneId)
    else:
        # Create new requests
        createRequests(requests, num_requests, args.doDryRun, args.useDev)

if __name__ == '__main__':
    main()
