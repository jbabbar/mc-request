import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *

generator = cms.EDFilter("Pythia8GeneratorFilter",
	comEnergy = cms.double(13000.0),
	pythiaHepMCVerbosity = cms.untracked.bool(False),
	pythiaPylistVerbosity = cms.untracked.int32(1),
	PythiaParameters = cms.PSet(
		pythia8CommonSettingsBlock,
		pythia8CP5SettingsBlock,
		processParameters = cms.vstring(
			'6:m0 = 175.',
			'ExcitedFermion:qqbar2muStarmu = on',
			'ExcitedFermion:Lambda= 10000',
			'4000013:m0 = 3500',
			'4000013:onMode = off',
			'4000013:onIfMatch = 13 23',
			'23:onMode = off',
			'23:onIfAny = 1 2 3 4 5',
			'Next:numberShowInfo = 1',
			'Next:numberShowProcess = 2',
			'Next:numberShowEvent = 2',
		),
		parameterSets = cms.vstring('pythia8CommonSettings',
									'pythia8CP5Settings',
									'processParameters')
       )
)

ProductionFilterSequence = cms.Sequence(generator)

