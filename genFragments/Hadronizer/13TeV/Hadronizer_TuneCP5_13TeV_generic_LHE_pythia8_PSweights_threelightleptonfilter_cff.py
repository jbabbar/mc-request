import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter("Pythia8HadronizerFilter",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13000.),
    PythiaParameters = cms.PSet(
        pythia8CommonSettingsBlock,
        pythia8CP5SettingsBlock,
        pythia8PSweightsSettingsBlock,
        parameterSets = cms.vstring('pythia8CommonSettings',
                                    'pythia8CP5Settings',
                                    'pythia8PSweightsSettings',
                                    )
    )
)

trileptonfilter = cms.EDFilter("MCMultiParticleFilter",
    PtMin = cms.vdouble(5., 5.),
    ParticleID = cms.vint32(11, 13),
    NumRequired = cms.int32(3),
    AcceptMore = cms.bool(True),
    EtaMax = cms.vdouble(3., 3.),
    Status = cms.vint32(1, 1)
)

ProductionFilterSequence = cms.Sequence(generator*trileptonfilter)

