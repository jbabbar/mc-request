# GS fragment

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter("Pythia8HadronizerFilter",
                         maxEventsToPrint = cms.untracked.int32(1),
                         pythiaPylistVerbosity = cms.untracked.int32(1),
                        filterEfficiency = cms.untracked.double(1.0),
                         pythiaHepMCVerbosity = cms.untracked.bool(False),
                         comEnergy = cms.double(13000.),
                         PythiaParameters = cms.PSet(
                             pythia8CommonSettingsBlock,
                             pythia8CP5SettingsBlock,
                             pythia8PSweightsSettingsBlock,
                             pythia8aMCatNLOSettingsBlock,
                             processParameters = cms.vstring(
                                 'JetMatching:setMad = off',
                                 'JetMatching:scheme = 1',
                                 'JetMatching:merge = on',
                                 'JetMatching:jetAlgorithm = 2',
                                 'JetMatching:etaJetMax = 999.',
                                 'JetMatching:coneRadius = 1.',
                                 'JetMatching:slowJetPower = 1',
                                 'JetMatching:qCut = 30.', #this is the actual merging scale
                                 'JetMatching:doFxFx = on',
                                 'JetMatching:qCutME = 15.',#this must match the ptj cut in the lhe generation step
                                 'JetMatching:nQmatch = 5', #4 corresponds to 4-flavour scheme (no matching of b-quarks), 5 for 5-flavour sch
eme
                                 'JetMatching:nJetMax = 2', #number of partons in born matrix element for highest multiplicity
                             ),
                             parameterSets = cms.vstring('pythia8CommonSettings',
                                                         'pythia8CP5Settings',
                                                         'pythia8PSweightsSettings',
                                                         'pythia8aMCatNLOSettings',
                                                         'processParameters',
                                                     )
                         )
                     )
#Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_qCutME15_max2j_LHE_pythia8_cff.py
