import FWCore.ParameterSet.Config as cms

from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *
from Configuration.Generator.Pythia8aMCatNLOSettings_cfi import *
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import *

generator = cms.EDFilter("Pythia8HadronizerFilter",
    maxEventsToPrint = cms.untracked.int32(1),
    pythiaPylistVerbosity = cms.untracked.int32(1),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(13000.),
    PythiaParameters = cms.PSet(
        pythia8CommonSettingsBlock,
        pythia8CP5SettingsBlock,
        pythia8aMCatNLOSettingsBlock,
        pythia8PSweightsSettingsBlock,
        processParameters = cms.vstring(
            'TimeShower:nPartonsInBorn = 1', #number of coloured particles (before resonance decays) in born matrix element
        ),
        parameterSets = cms.vstring('pythia8CommonSettings',
                                    'pythia8CP5Settings',
                                    'pythia8aMCatNLOSettings',
                                    'processParameters',
                                    'pythia8PSweightsSettings'
                                    )
    )
)

from PhysicsTools.HepMCCandAlgos.genParticles_cfi import genParticles
from PhysicsTools.JetMCAlgos.TauGenJets_cfi import tauGenJets
from PhysicsTools.JetMCAlgos.TauGenJetsDecayModeSelectorAllHadrons_cfi import tauGenJetsSelectorAllHadrons

genParticles.src = cms.InputTag("generator", "unsmeared")

tauGenJets.GenParticles = cms.InputTag("genParticles")
tauGenJets.includeNeutrinos = cms.bool(False)

genVisTauSelector = cms.EDFilter("CandViewSelector",
    src = cms.InputTag("tauGenJetsSelectorAllHadrons"),
    cut = cms.string("pt > 18")
  )             
        
genVisTauFilter = cms.EDFilter("CandViewCountFilter",
     src = cms.InputTag("genVisTauSelector"),
     minNumber = cms.uint32(1),
  )

lightleptonfilter = cms.EDFilter("MCMultiParticleFilter",
    PtMin = cms.vdouble(15., 15.),
    ParticleID = cms.vint32(11, 13),
    NumRequired = cms.int32(1),
    AcceptMore = cms.bool(True),
    EtaMax = cms.vdouble(3., 3.),
    Status = cms.vint32(1, 1)
)

ProductionFilterSequence = cms.Sequence(generator*genParticles*tauGenJets*tauGenJetsSelectorAllHadrons*genVisTauSelector*genVisTauFilter*lightleptonfilter)

