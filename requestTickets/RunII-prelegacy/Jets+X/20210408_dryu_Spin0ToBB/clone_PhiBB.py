import sys
sys.path.append('/afs/cern.ch/cms/PPD/PdmV/tools/McM/')
from rest import McM
from json import dumps

mcm = McM(dev=False)

input_prepids = {
	"2016": ["EXO-RunIISummer15wmLHEGS-10771", "EXO-RunIISummer15wmLHEGS-10772", "EXO-RunIISummer15wmLHEGS-10773", "EXO-RunIISummer15wmLHEGS-10774", "EXO-RunIISummer15wmLHEGS-10775", "EXO-RunIISummer15wmLHEGS-10776", "EXO-RunIISummer15wmLHEGS-10777", "EXO-RunIISummer15wmLHEGS-10778", "EXO-RunIISummer15wmLHEGS-10779", "EXO-RunIISummer15wmLHEGS-10780", "EXO-RunIISummer15wmLHEGS-10781", "EXO-RunIISummer15wmLHEGS-10782", "EXO-RunIISummer15wmLHEGS-10783", "EXO-RunIISummer15wmLHEGS-10784", "EXO-RunIISummer15wmLHEGS-10785", "EXO-RunIISummer15wmLHEGS-10786", "EXO-RunIISummer15wmLHEGS-10787", "EXO-RunIISummer15wmLHEGS-10788", "EXO-RunIISummer15wmLHEGS-10789", "EXO-RunIISummer15wmLHEGS-10790", "EXO-RunIISummer15wmLHEGS-10791", "EXO-RunIISummer15wmLHEGS-10792", "EXO-RunIISummer15wmLHEGS-10793", "EXO-RunIISummer15wmLHEGS-10794", "EXO-RunIISummer15wmLHEGS-10795", "EXO-RunIISummer15wmLHEGS-10796", "EXO-RunIISummer15wmLHEGS-10797", "EXO-RunIISummer15wmLHEGS-10798"]
	"2017": ["EXO-RunIIFall17wmLHEGS-04938", "EXO-RunIIFall17wmLHEGS-04939", "EXO-RunIIFall17wmLHEGS-04940", "EXO-RunIIFall17wmLHEGS-04941", "EXO-RunIIFall17wmLHEGS-04942", "EXO-RunIIFall17wmLHEGS-04943", "EXO-RunIIFall17wmLHEGS-04944", "EXO-RunIIFall17wmLHEGS-04945", "EXO-RunIIFall17wmLHEGS-04946", "EXO-RunIIFall17wmLHEGS-04947", "EXO-RunIIFall17wmLHEGS-04948", "EXO-RunIIFall17wmLHEGS-04949", "EXO-RunIIFall17wmLHEGS-04950", "EXO-RunIIFall17wmLHEGS-04951", "EXO-RunIIFall17wmLHEGS-04952", "EXO-RunIIFall17wmLHEGS-04953", "EXO-RunIIFall17wmLHEGS-04954", "EXO-RunIIFall17wmLHEGS-04955", "EXO-RunIIFall17wmLHEGS-04956", "EXO-RunIIFall17wmLHEGS-04957", "EXO-RunIIFall17wmLHEGS-04958", "EXO-RunIIFall17wmLHEGS-04959", "EXO-RunIIFall17wmLHEGS-04960", "EXO-RunIIFall17wmLHEGS-04961", "EXO-RunIIFall17wmLHEGS-04962", "EXO-RunIIFall17wmLHEGS-04963", "EXO-RunIIFall17wmLHEGS-04964", "EXO-RunIIFall17wmLHEGS-04965"], 
	"2018": ["EXO-RunIIFall18wmLHEGS-04945", "EXO-RunIIFall18wmLHEGS-04946", "EXO-RunIIFall18wmLHEGS-04947", "EXO-RunIIFall18wmLHEGS-04948", "EXO-RunIIFall18wmLHEGS-04949", "EXO-RunIIFall18wmLHEGS-04950", "EXO-RunIIFall18wmLHEGS-04951", "EXO-RunIIFall18wmLHEGS-04952", "EXO-RunIIFall18wmLHEGS-04953", "EXO-RunIIFall18wmLHEGS-04954", "EXO-RunIIFall18wmLHEGS-04955", "EXO-RunIIFall18wmLHEGS-04956", "EXO-RunIIFall18wmLHEGS-04957", "EXO-RunIIFall18wmLHEGS-04958", "EXO-RunIIFall18wmLHEGS-04959", "EXO-RunIIFall18wmLHEGS-04960", "EXO-RunIIFall18wmLHEGS-04961", "EXO-RunIIFall18wmLHEGS-04962", "EXO-RunIIFall18wmLHEGS-04963", "EXO-RunIIFall18wmLHEGS-04964", "EXO-RunIIFall18wmLHEGS-04965", "EXO-RunIIFall18wmLHEGS-04966", "EXO-RunIIFall18wmLHEGS-04967", "EXO-RunIIFall18wmLHEGS-04968", "EXO-RunIIFall18wmLHEGS-04969", "EXO-RunIIFall18wmLHEGS-04970", "EXO-RunIIFall18wmLHEGS-04971", "EXO-RunIIFall18wmLHEGS-04972"], 
}
 
for input_prepid in input_prepids:
	print("\t*** {} ***".format(input_prepid))
	request = mcm.get('requests', input_prepid)
	new_fragment = request["fragment"]

	# Change gridpack path
	new_fragment = new_fragment.replace("/cvmfs/cms.cern.ch/phys_generator/gridpacks/2017/13TeV/madgraph/V5_2.6.0/Spin0ToBB/Spin0ToBB", 
								"/cvmfs/cms.cern.ch/phys_generator/gridpacks/2017/13TeV/madgraph/V5_2.6.0/Spin0ToBB/v2/Spin0ToBB")

	if test:
		print(new_fragment)
	else:
		request["fragment"] = new_fragment
		clone_answer = mcm.clone_request(request)
		if clone_answer.get('results'):
		    print('Clone PrepID: %s' % (clone_answer['prepid']))
		    print clone_answer
		else:
		    print('Something went wrong while cloning a request. %s' % (dumps(clone_answer)))

