import os
import sys
import csv
import re
import fnmatch

def checkInputCSV(requests, fields, testdir, file_in):

    print "Checking MC sample request input CSV file."
    print "Dataset naming rules in McM : PROCESS_RANGETYPE-RANGELOWToRANGEHIGH_FILTER_TUNE_COMENERGY-GENERATOR"

    if "/" in file_in:
        os.system("cp {0} ./{1}".format(file_in, file_in.split("/")[-1]))
        file_in = file_in.split("/")[-1]

    firstRowCSV = []
    field_essentials_CSV = ["dataset", "fragment", "events", "generator"]
    field_additionals_CSV = ["gridpack"]#, "time per event","size per event"]

    for field in field_essentials_CSV:
        if field in fields.keys():
            firstRowCSV.append(field)
        else:
            print "Error: {0} is minimal list of needed information. Missing {1}.".format(field_essentials_CSV, field)
            sys.exit(1)
    for field in field_additionals_CSV:
        if field in fields.keys():
            firstRowCSV.append(field)

    errors = 0

    for reqFields in requests:
        reqRowDataSetName = reqFields.getDataSetName()
        print "Scanning CSV file in line {0}.".format(reqRowDataSetName)

        errorsGen,replaceGen = checkGenSynonym(reqFields.getGen(), reqFields.useGridpack())
        errors += errorsGen
        errorsDataSetName = checkDataSetName(reqFields.getDataSetName(), reqFields.getMcMFrag(), replaceGen, reqFields.useGridpack())
        errors += errorsDataSetName

    print "Found {0} errors in \"{1}\".".format(errors, file_in)

    if (errors == 0):
        return True
    else:
        return False


# Check and modify the dataset names
def checkDataSetName(datasetname, fragment, generator, gridpack):

    errors = 0

    psgenerator,tune,regex_tune = checkFragment(fragment)

    if not (tune == "NO_TUNES"):
        if not re.compile(regex_tune).search(fragment) is None:
            tune = tune+str(re.compile(regex_tune).search(fragment).group(1))

    if (tune == None):
        errors += 1
        print " >>       Error : [fragment] field is not filled correctly. Tune setting is not recognised."
    else:
        if gridpack:
            generator = "{0}-{1}".format(generator,psgenerator)
        else:
            generator = psgenerator

        if (tune == "NO_TUNES"):
            if (datasetname.endswith("_13TeV-{0}".format(generator)) or datasetname.endswith("_14TeV-{0}".format(generator))):
                print " >>       [dataset] field is filled correctly."
            else:
                errors += 1
                print " >>       Error : [dataset] field is not filled correctly. \"{0}\" should end with \"_1XTeV-{1}\".".format(datasetname,generator)
        else:
            if (datasetname.endswith("_{0}_13TeV-{1}".format(tune,generator)) or datasetname.endswith("_{0}_14TeV-{1}".format(tune,generator))):
                print " >>       [dataset] field is filled correctly."
            else:
                errors += 1
                print " >>       Error : [dataset] field is not filled correctly. \"{0}\" should end with \"_{1}_1XTeV-{2}\".".format(datasetname,tune,generator)

    return errors

# Check synonyms for the generator
def checkGenSynonym(generator, gridpack):

    errors = 0

    genSynonymsMEList={# List the ME generators first and then the PS (ME+PS) generators
        "madgraph":["*madgraph*", "*mg*", "*amcatnlo*"], #ME
        "powheg":["*powheg*"], #ME
    }

    genSynonymsPSList={# List the ME generators first and then the PS (ME+PS) generators
        "sherpa":["*sherpa*"], #ME+PS
        "pythia8":["*pythia*"], #ME+PS
        "herwig":["*herwig*", "*hw*"], #ME+PS
    }

    genReNames = []

    if gridpack:
        genSynMEFound = False
        genSynMEMatch = False
        for genReName,genSynonyms in genSynonymsMEList.iteritems():# First look for ME only generators
            for genSynonym in genSynonyms:
                genFound = fnmatch.filter([generator], genSynonym)
                if genFound:
                    genSynMEFound = True
                    genReNames.append(genReName)
                    if genReName == generator:
                        genSynMEMatch = True

    genSynFound = False
    genSynMatch = False
    for genReName,genSynonyms in genSynonymsPSList.iteritems():# Then look for (ME+)PS generators
        for genSynonym in genSynonyms:
            genFound = fnmatch.filter([generator], genSynonym)
            if genFound:
                genSynFound = True
                genReNames.append(genReName)
                if not gridpack:
                    if genReName == generator:
                        genSynMatch = True

    if not (generator == generator.lower()):
        errors += 1
        print " >>       Error : [generator] field should be lowercased. Please change \"{0}\" to \"{1}\".".format(generator,generator.lower())
        return errors, ""

    if gridpack:
        if genSynMEFound:
            if genSynMEMatch:
                print " >>       [generator] field is filled correctly."
            else:
                errors += 1
                print " >>       Error : [generator] field is not filled correctly. Please change \"{0}\" to \"{1}\".".format(generator,genReNames[0])
        else:
            errors += 1
            print " >>       Error : [generator] field is not filled correctly. Unrecognised generator \"{0}\". Please change it to among below : .".format(generator)
            for genReName,genSynonyms in genSynonymsMEList.iteritems():
                print " >>              {0}".format(genReName)
    else:
        if genSynFound:
            if genSynMatch:
                print " >>       [generator] field is filled correctly."
            else:
                errors += 1
                print " >>       Error : [generator] field is not filled correctly. Please change \"{0}\" to \"{1}\".".format(generator,genReNames[0])
        else:
            errors += 1
            print " >>       Error : [generator] field is not filled correctly. Unrecognised generator \"{0}\". Please change it to among below : .".format(generator)
            for genReName,genSynonyms in genSynonymsList.iteritems():
                print " >>              {0}".format(genReName)

    try:
        return errors, "{0}-{1}".format(genReNames[0],genReNames[1])
    except:
        try:
            return errors, "{0}".format(genReNames[0])
        except:
            return errors, ""

def checkFragment(fragment):

    psgenerator = ""

    # Check the tuning settings for Parton Showers
    if "Pythia" in fragment:
        psgenerator = "pythia8"
        tune = "TuneCP"
        regex_tune = ".*PythiaCP(\d*)Settings.*"
    elif "Herwig" in fragment:
        psgenerator = "herwig"
        tune = "TuneCH"
        regex_tune = ".*Herwig7CH(\d*)TuneSettings.*"
    elif "Sherpa" in fragment:
        psgenerator = "sherpa"
        tune = "NO_TUNES"
        regex_tune = "NO_TUNES"

    if "Tauola" in fragment:
        psgenerator += "-tauola"

    return psgenerator,tune,regex_tune 

def checkGridpack(requests):

    checkGridpacks = 0
    for reqFields in requests:
        reqRowDataSetName = reqFields.getDataSetName()

        useGridpack = reqFields.useGridpack()        

        if(useGridpack):
            print "Checking gridpack location in CSV file in line {0}.".format(reqRowDataSetName)

            pathToGridpack = reqFields.getGridpack()

            if "/cvmfs/cms.cern.ch/phys_generator/gridpacks/" in pathToGridpack:
                print " >>       [gridpack] field is filled correctly."
            else:
                print " >>       Error : [gridpack] field is not filled correctly. Gridpacks should be placed in /cvmfs/cms.cern.ch/phys_generator/gridpacks/."
                checkGridpacks += 1

    return checkGridpacks
