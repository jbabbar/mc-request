# exo-SampleRequests

## Instructions on how to create MC sample requests for the Exotica PAG. 

1. Determine the directory to create your request directory within. The directory is based on the era you want your samples to be produced in (RunII UL, RunIII, etc.) and which EXO subgroup you are a part of (Jets+X, LLP, etc.)
2. Create your request directory. Your request directory should have the following format, DATE_CERNID_SAMPLEINFO, where DATE should have the format YYYYMMDD and SAMPLEINFO is the type of sample, like DarkHiggsToWWToLNuJJ.
3. Within your request directory create a request.csv file with your request info (necessary request info explained here: https://twiki.cern.ch/twiki/bin/view/Main/ExoMCInstructions) divided with commas, similar to the example: https://gitlab.cern.ch/cms-exo-mci/EXO-MCsampleRequests/-/blob/master/requestTickets/RunII-prelegacy/Jets+X/20210101_jihun.k_TypeIHeavyN-dilepton/request.csv
4. Within your request directory create a request.adoc file with the exact lines in https://gitlab.cern.ch/cms-exo-mci/EXO-MCsampleRequests/-/raw/master/requestTickets/RunII-prelegacy/Jets+X/20210101_jihun.k_TypeIHeavyN-dilepton/request.adoc copied and pasted.
5. Create your hadronizer python file in the approriate genFragments/Hadronizer/ directory.
6. If the request is a Pythia or Sherpa request that only runs from python files, then place the python file in the approriate genFragments/Generator/ directory.
7. Make a pull request.

